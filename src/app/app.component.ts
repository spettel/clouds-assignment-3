import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {SongsService} from "./songs.service";
import {SongDto} from "./dto/song";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {NewsongComponent} from "./dialog/newsong/newsong.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'ass3-app';

  songs: SongDto[] = []
  displayedColumns: string[] = ["id", "name", "artist", "genre", "downloads", "action"]

  constructor(private songsService: SongsService, private dialog: MatDialog,
              private snackBar : MatSnackBar) {
  }

  ngOnInit(): void {
    this.loadAllSongs()
  }

  updateSongs()
  {
    const clonedArray : SongDto[]= []; this.songs.map(val => clonedArray.push(Object.assign({}, val)));
    this.songs = clonedArray;
  }

  loadAllSongs()
  {
    this.songsService.getAllSongs().subscribe(
      res => {
        this.songs = res.Items;
      },
      (error: HttpErrorResponse) => {
        this.openSnackBar("Could not load all songs", "Dismiss")
      }
    )
  }

  assertSongDto(element : SongDto) : SongDto {
    return element
  }

  openSongDialog(songDto: SongDto | null)
  {
    var dialogRef = this.dialog.open(NewsongComponent, {
      disableClose: false,
      data: {
        song: songDto
      }
    });
    dialogRef.afterClosed().subscribe(
      (result: SongDto) => {
        if (result != null) {
          if (result.id != "") {
            this.songsService.updateSong(result).subscribe(
              result => {
                this.openSnackBar("Song edited successfully", "Dismiss")
                let idx = this.songs.findIndex(x => x.id == result.success.id)


                console.log(result.success as SongDto)
                console.log("idx: " + idx)

                this.songs[idx] = result.success as SongDto
                this.updateSongs()
              },
              error => {
                this.openSnackBar("Could not update song", "Dismiss")
                console.log(error)
              }
            )
          } else {
            this.songsService.addSong(result).subscribe(
              result => {
                this.openSnackBar("Song added successfully", "Dismiss")
                this.songs.push(result.success as SongDto)
                this.updateSongs()
              },
              error => {
                this.openSnackBar("Could not add song", "Dismiss")
                console.log(error)
              }
            )
          }
        }
      }
    )

  }

  addSongButton()
  {
    this.openSongDialog(null)
  }

  editSong(songDto: SongDto) {
    this.openSongDialog(songDto)
  }

  deleteSong(songDto: SongDto) {
    this.songsService.deleteSong(songDto).subscribe(
      result =>
      {
        this.openSnackBar("Song deleted successfully", "Dismiss")
        let index = this.songs.indexOf(songDto)
        this.songs.splice(index,1)
        this.updateSongs()
      },
      error => {
        this.openSnackBar("Could not delete song", "Dismiss")
        console.log(error)
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

}
