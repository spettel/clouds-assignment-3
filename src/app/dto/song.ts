export class SongDto {
  id: string | undefined;
  name: string | undefined;
  artist: string | undefined;
  genre: string | undefined;
  downloads: number | undefined;

  public equals(obj: SongDto) : boolean {
    return this.id === obj.id;
  }

}
