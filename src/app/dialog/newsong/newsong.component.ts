import {Component, Inject, INJECTOR, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {SongsService} from "../../songs.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {SongDto} from "../../dto/song";

@Component({
  selector: 'app-newsong',
  templateUrl: './newsong.component.html',
  styleUrls: ['./newsong.component.css']
})
export class NewsongComponent {

  form : FormGroup

  id = '';
  name = '';
  genre = '' ;
  artist = '';
  downloads = 0;

  numRegex = "[0-9]*"

  constructor(private songService : SongsService, private fb: FormBuilder, private dialogRef: MatDialogRef<NewsongComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {

    if (data.song as SongDto != null)
    {
      this.id = data.song.id;
      this.name = data.song.name;
      this.genre = data.song.genre;
      this.artist = data.song.artist;
      this.downloads = data.song.downloads;
    }
    this.form = this.fb.group({
      id : new FormControl({value: this.id, disabled: true}, Validators.required),
      name: new FormControl(this.name),
      genre: new FormControl(this.genre),
      artist: new FormControl(this.artist),
      downloads: new FormControl(this.downloads, [Validators.min(0), Validators.pattern(this.numRegex)])
    })
  }

  cancel() {
    this.dialogRef.close()
  }

  save() {
    if (this.form?.valid)
    {
      var newSong = new SongDto()
      newSong.id = this.form.get('id')?.value;
      newSong.artist = this.form.get('artist')?.value;
      newSong.downloads = this.form.get('downloads')?.value;
      newSong.genre = this.form.get('genre')?.value;
      newSong.name = this.form.get('name')?.value;
      this.dialogRef.close(newSong)
    }
    else {
      this.form?.markAllAsTouched()
    }
  }
}
