import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Observable} from "rxjs";
import {SongDto} from "./dto/song";

@Injectable({
  providedIn: 'root'
})
export class SongsService {

  url = environment.apiURL

  constructor(private http : HttpClient) { }

  getAllSongs() : Observable<any>
  {
    return this.http.get<SongDto[]>(this.url)
  }

  getSong(id : string) : Observable<any>
  {
    return this.http.get<SongDto>(`${this.url}/${id}`)
  }

  addSong(song: SongDto) : Observable<any> {
    return this.http.put(this.url, song)
  }

  updateSong(song: SongDto) : Observable<any>{
    return this.http.put(`${this.url}/${song.id}`, song)
  }

  deleteSong(song: SongDto) : Observable<any>{
    return this.http.delete(`${this.url}/${song.id}`)
  }

}
